# ChatServer

Chat is a channel based communication tool. It supports multiple channels for users communications.
A user can only join one channel at a time, when they join another they leave their current channel.
The same user can auth twice from different devices, join to different channel and on both them they
should be able to receive messages.

## ChatServer handles the following commands:

> /login <name> <password>

If the user doesn't exist, create profile else login, after login join to last connected
channel (if the current channel's client limit exceeded, user keep connected, but without active
channel).

> /join <channel>

Try to join a channel (max 10 active clients per channel). If channel's client limit
exceeded - error occurred, otherwise user joins channel and received last N messages of activity.

> /leave

Leave current channel.

> /disconnect

Close connection to the server.

> /list

Get list of channels.

> /users

Get list of unique users in current channel.

> *text message terminated with CR*

Send message to the current channel to all connected users in this channel.

## Build

Run in command line: `mvn clean install`

## Run

Run in command line: `mvn exec:java`

## Client

For Windows users there are additional steps:

You can use Microsoft telnet cmd tool, but it's not recommended, due to backspace issues

Recommended to use wsdl ubuntu terminal:
1. Open wsdl ubuntu terminal
2. Run `nslookup "$(hostname).local"`. You will get similar line: `Server: 170.30.80.2`
3. Use this ip address to connect to server from ubuntu command line

For Linux users:
1. Install `telnet` cmd tool (if you don't have yet)
2. Run `telnet`
3. Now you are able to connect to the server
4. Run `open localhost [PORT NUMBER, 23 by default]` use `170.30.80.2` if you use wsdl terminal
5. You should get message: `Welcome to chat server!`
6. Run commands from description list

Known issue: if we use default 23 port, the first command is not handled properly
