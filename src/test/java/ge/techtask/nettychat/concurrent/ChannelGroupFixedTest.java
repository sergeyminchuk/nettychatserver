package ge.techtask.nettychat.concurrent;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.DefaultChannelId;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.junit.jupiter.api.RepeatedTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ge.techtask.nettychat.utils.RandomUtils.getRandomNumberBetween;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

public class ChannelGroupFixedTest {

	private static final int N_THREADS = 50;
	private static final int CHANNEL_LIMIT = 10;
	private static final int INSERT_CNT = 15000;
	private static final int REMOVE_CNT = 5000;

	private static final int MIN_DELAY = 20;
	private static final int MAX_DELAY = 50;

	@RepeatedTest(10)
	public void testGroupNonThreadSafeFixedSize() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(INSERT_CNT + REMOVE_CNT);

		ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
		List<Channel> channels = generateChannels(INSERT_CNT);

		AtomicBoolean failed = new AtomicBoolean(false);
		ScheduledExecutorService eSch1 = Executors.newScheduledThreadPool(N_THREADS);

		IntStream.range(0, INSERT_CNT).forEach((i) -> {
			eSch1.schedule(() -> {
				if (channelGroup.size() < CHANNEL_LIMIT) {
					channelGroup.add(channels.get(i));
				}

				if (channelGroup.size() > CHANNEL_LIMIT) {
					failed.set(Boolean.TRUE);
				}
				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY, MAX_DELAY), TimeUnit.MILLISECONDS);
		});

		List<Channel> shuffled = new ArrayList<>(channels);
		Collections.shuffle(shuffled);

		ScheduledExecutorService eSch2 = Executors.newScheduledThreadPool(N_THREADS);

		IntStream.range(0, REMOVE_CNT).forEach((i) -> {
			eSch2.schedule(() -> {
				channelGroup.remove(channels.get(i));
				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY, MAX_DELAY), TimeUnit.MILLISECONDS);
		});

		latch.await();
		if (failed.get() == Boolean.FALSE) {
			fail();
		}
	}

	@RepeatedTest(10)
	public void testGroupThreadSafeFixedSize() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(INSERT_CNT + REMOVE_CNT);

		ChannelGroupFixed channelGroup = new ChannelGroupFixed(CHANNEL_LIMIT);
		List<Channel> channels = generateChannels(INSERT_CNT);

		AtomicBoolean failed = new AtomicBoolean(false);
		ScheduledExecutorService eSch1 = Executors.newScheduledThreadPool(N_THREADS);

		IntStream.range(0, INSERT_CNT).forEach((i) -> {
			eSch1.schedule(() -> {
				try {
					channelGroup.add(channels.get(i));
				} catch (GroupLimitExceededException ignored) {
				}

				if (channelGroup.size() > CHANNEL_LIMIT) {
					failed.set(Boolean.TRUE);
				}
				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY, MAX_DELAY), TimeUnit.MILLISECONDS);
		});

		List<Channel> shuffled = new ArrayList<>(channels);
		Collections.shuffle(shuffled);

		ScheduledExecutorService eSch2 = Executors.newScheduledThreadPool(N_THREADS);

		IntStream.range(0, REMOVE_CNT).forEach((i) -> {
			eSch2.schedule(() -> {
				channelGroup.remove(channels.get(i));
				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY, MAX_DELAY), TimeUnit.MILLISECONDS);
		});

		latch.await();
		if (failed.get() == Boolean.TRUE) {
			fail();
		}
	}

	@RepeatedTest(10)
	public void testGroupThreadSafeFixedSizeExceptionCount() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(INSERT_CNT);

		ChannelGroupFixed channelGroup = new ChannelGroupFixed(CHANNEL_LIMIT);
		List<Channel> channels = generateChannels(INSERT_CNT);

		AtomicBoolean failed = new AtomicBoolean(false);
		ScheduledExecutorService eSch1 = Executors.newScheduledThreadPool(N_THREADS);

		List<GroupLimitExceededException> exceededExceptions = new CopyOnWriteArrayList<>();

		IntStream.range(0, INSERT_CNT).forEach((i) -> {
			eSch1.schedule(() -> {
				try {
					channelGroup.add(channels.get(i));
				} catch (GroupLimitExceededException e) {
					exceededExceptions.add(e);
				}

				if (channelGroup.size() > CHANNEL_LIMIT) {
					failed.set(Boolean.TRUE);
				}
				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY, MAX_DELAY), TimeUnit.MILLISECONDS);
		});

		latch.await();
		if (failed.get() == Boolean.TRUE) {
			fail();
		}

		assertEquals(INSERT_CNT - CHANNEL_LIMIT, exceededExceptions.size());
	}

	private List<Channel> generateChannels(int attempts) {
		return IntStream.range(0, attempts).mapToObj((i) -> {
			Channel channel = mock(Channel.class);
			when(channel.id()).thenReturn(DefaultChannelId.newInstance());
			when(channel.closeFuture()).thenReturn(mock(ChannelFuture.class));
			return channel;
		}).collect(Collectors.toList());
	}
}
