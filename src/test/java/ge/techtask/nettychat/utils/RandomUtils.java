package ge.techtask.nettychat.utils;

import java.util.Random;

public class RandomUtils {
	public static int getRandomNumberBetween(int min, int max) {
		Random random = new Random();
		return random.nextInt(max - min) + min;
	}
}
