package ge.techtask.nettychat.data;

import ge.techtask.nettychat.server.Command;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.DecoderException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.charset.StandardCharsets;

import static ge.techtask.nettychat.server.Command.MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RequestDataDecoderTest {

	private EmbeddedChannel channel;

	@BeforeEach
	public void setUp() {
		channel = new EmbeddedChannel(new RequestDataDecoder());
	}

	@ParameterizedTest
	@EnumSource(Command.class)
	public void testAllCommands(Command command) {
		channel.writeInbound(getByteBuf(command.getCommand()));

		RequestData requestData = channel.readInbound();
		assertEquals(command, requestData.getCommand());
		assertEquals("", requestData.getMessage());
	}

	@ParameterizedTest
	@ValueSource(strings = {"message", "message with spaces", "34132434", "   ", "$^%@#&!@^#", "ଶ"})
	public void testCommandWithMessage(String message) {
		channel.writeInbound(getByteBuf(Command.LOGIN.getCommand() + ' ' + message));

		RequestData requestData = channel.readInbound();
		assertEquals(Command.LOGIN, requestData.getCommand());
		assertEquals(message, requestData.getMessage());
	}

	@ParameterizedTest
	@ValueSource(strings = {"msg", "message with spaces", "12312831", "!@#$%^&*()_+", "ଶ"})
	public void testMessageCommand(String message) {
		channel.writeInbound(getByteBuf(message));

		RequestData requestData = channel.readInbound();
		assertEquals(MESSAGE, requestData.getCommand());
		assertEquals(message, requestData.getMessage());
	}

	@ParameterizedTest
	@ValueSource(strings = {"/", "/qwerty", "/\\", "/b", "/logind", "/lоgin"})
	public void testInvalidCommands(String message) {
		try {
			channel.writeInbound(getByteBuf(message));
		} catch (DecoderException e) {
			assertTrue(e.getMessage().contains("invalid command: " + message));
		}
	}

	private static ByteBuf getByteBuf(String message) {
		return Unpooled.copiedBuffer(message, StandardCharsets.UTF_8);
	}
}
