package ge.techtask.nettychat.data;

import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.string.StringDecoder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ResponseLineEncoderTest {

	private static final String LINE_TERMINATOR = "\n";
	private EmbeddedChannel channel;

	@BeforeEach
	public void setUp() {
		channel = new EmbeddedChannel(new StringDecoder(), new ResponseLineEncoder(LINE_TERMINATOR));
	}

	@ParameterizedTest
	@ValueSource(strings = {" ", "message", "message with spaces", "318231232", "  sdf  ", "sdf\n", "ଶ"})
	public void testLineEncoder(String message) {
		channel.writeOutbound(message);

		ByteBuf outBuf = channel.readOutbound();
		String resultMsg = outBuf.toString(StandardCharsets.UTF_8);

		int allButLastLen = resultMsg.length() - 1;
		assertEquals(message, resultMsg.substring(0, allButLastLen));
		assertEquals(LINE_TERMINATOR, String.valueOf(resultMsg.charAt(allButLastLen)));
	}
}
