package ge.techtask.nettychat.dao;

import ge.techtask.nettychat.data.MessageData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import static ge.techtask.nettychat.utils.RandomUtils.getRandomNumberBetween;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageDaoImplTest {

	private static final int N_THREADS_IN = 50;
	private static final int N_THREADS_OUT = 50;
	private static final int INSERT_CNT = 300000;
	private static final int RETRIEVE_CNT = 50000;

	private static final int MIN_DELAY_INSERT = 100;
	private static final int MAX_DELAY_INSERT = 200;

	private static final int MIN_DELAY_RETRIEVE = 100;
	private static final int MAX_DELAY_RETRIEVE = 200;
	private static final int MESSAGE_N = 50;

	private static final String CHANNEL_NAME = "channelName";

	private MessageDaoImpl service;

	@BeforeEach
	public void setUp() {
		service = new MessageDaoImpl();
	}

	@Test
	public void threadSafePutAndRetrieveMessages() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(INSERT_CNT + RETRIEVE_CNT);

		ScheduledExecutorService eSch1 = Executors.newScheduledThreadPool(N_THREADS_IN);

		List<MessageData> messageDataList = new ArrayList<>();
		List<List<MessageData>> messagesChunks = new CopyOnWriteArrayList<>();

		Lock insertMessageLock = new ReentrantLock();
		IntStream.range(0, INSERT_CNT).forEach((i) -> {
			eSch1.schedule(() -> {
				MessageData md = MessageData.builder().message(String.valueOf(i)).build();

				insertMessageLock.lock();

				try {
					messageDataList.add(md);
					service.save(CHANNEL_NAME, md);
				} finally {
					insertMessageLock.unlock();
				}

				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY_INSERT, MAX_DELAY_INSERT), TimeUnit.MILLISECONDS);
		});

		ScheduledExecutorService eSch2 = Executors.newScheduledThreadPool(N_THREADS_OUT);

		IntStream.range(0, RETRIEVE_CNT).forEach((i) -> {
			eSch2.schedule(() -> {
				List<MessageData> actualMessages = service.findLastNMessages(CHANNEL_NAME, MESSAGE_N);
				messagesChunks.add(actualMessages);

				latch.countDown();
			}, getRandomNumberBetween(MIN_DELAY_RETRIEVE, MAX_DELAY_RETRIEVE), TimeUnit.MILLISECONDS);
		});

		latch.await();

		messagesChunks.forEach(subList -> {
			if (!subList.isEmpty()) {
				MessageData firstEl = subList.get(0);
				int ind = messageDataList.indexOf(firstEl);
				assertEquals(subList, messageDataList.subList(ind, ind + subList.size()));
			}
		});
	}
}
