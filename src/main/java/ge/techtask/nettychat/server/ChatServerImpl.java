package ge.techtask.nettychat.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChatServerImpl implements ChatServer {

	private final ChatChannelInitializer chatChannelInitializer;
	private final Environment env;

	@Override
	public void run() {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		try {
			ServerBootstrap sb = new ServerBootstrap();

			sb.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(chatChannelInitializer);

			sb.childOption(ChannelOption.SO_KEEPALIVE, true);

			int port = env.getRequiredProperty("port", Integer.class);
			ChannelFuture cf = sb.bind(port).sync();
			Channel channel = cf.channel();

			System.out.printf("Server starter at %d. Channel id: %s%n", port, channel.id());

			channel.closeFuture().sync();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}
}
