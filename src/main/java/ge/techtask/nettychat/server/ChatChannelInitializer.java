package ge.techtask.nettychat.server;

import ge.techtask.nettychat.data.RequestDataDecoder;
import ge.techtask.nettychat.data.ResponseLineEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChatChannelInitializer extends ChannelInitializer<SocketChannel> {

	private final ChatChannelHandler chatChannelHandler;
	private final Environment env;

	@Override
	protected void initChannel(SocketChannel channel) {
		ChannelPipeline cp = channel.pipeline();

		cp.addLast(new LineBasedFrameDecoder(env.getRequiredProperty("line.max.length", Integer.class)));
		cp.addLast(new RequestDataDecoder());
		cp.addLast(new ResponseLineEncoder());

		cp.addLast(chatChannelHandler);
	}
}
