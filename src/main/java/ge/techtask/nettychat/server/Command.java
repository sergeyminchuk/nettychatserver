package ge.techtask.nettychat.server;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Command {
	LOGIN("/login"),
	JOIN("/join"),
	LEAVE("/leave"),
	DISCONNECT("/disconnect"),
	LIST("/list"),
	USERS("/users"),
	MESSAGE("/message");

	private final String command;

	Command(String command) {
		this.command = command;
	}

	public static Command getCommandByName(String commandName) {
		Optional<Command> optionalCommand = Arrays.stream(values())
				.filter(v -> v.getCommand().equals(commandName))
				.findAny();

		if (optionalCommand.isEmpty()) {
			throw new UnsupportedOperationException("invalid command: " + commandName);
		}

		return optionalCommand.get();
	}
}
