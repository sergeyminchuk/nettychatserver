package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.concurrent.ChannelGroupFixed;
import ge.techtask.nettychat.data.ChatUser;
import ge.techtask.nettychat.data.MessageData;
import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.GroupChannelService;
import ge.techtask.nettychat.service.MessageService;
import ge.techtask.nettychat.utils.ChannelAttributeUtils;
import io.netty.channel.ChannelHandlerContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static ge.techtask.nettychat.server.ChannelAttribute.ACTIVE_USER;

@Component
@RequiredArgsConstructor
public class MessageCommand implements ChatCommand {

	private final GroupChannelService groupChannelService;
	private final MessageService messageService;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		String message = requestData.getMessage().strip();

		if (message.isEmpty() || message.isBlank()) {
			return;
		}

		var groupEntryOptional = groupChannelService.getCurrentGroup(ctx.channel());

		if (groupEntryOptional.isEmpty()) {
			throw new ChatCommandException("You are not a member of any of the channels.");
		}

		ChatUser activeUser = ChannelAttributeUtils.getChannelValue(ctx.channel(), ACTIVE_USER);

		MessageData messageData = MessageData
				.builder()
				.userName(activeUser.getName())
				.message(message)
				.build();

		var groupEntry = groupEntryOptional.get();
		String channelName = groupEntry.getKey();
		ChannelGroupFixed channelGroup = groupEntry.getValue();

		messageService.store(channelName, messageData);
		channelGroup.write(messageData.toString());
	}
}
