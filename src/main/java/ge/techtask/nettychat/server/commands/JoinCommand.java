package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.concurrent.GroupLimitExceededException;
import ge.techtask.nettychat.data.ChatUser;
import ge.techtask.nettychat.data.MessageData;
import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.GroupChannelService;
import ge.techtask.nettychat.service.MessageService;
import ge.techtask.nettychat.service.UserService;
import ge.techtask.nettychat.utils.ChannelAttributeUtils;
import io.netty.channel.ChannelHandlerContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static ge.techtask.nettychat.server.ChannelAttribute.ACTIVE_USER;
import static ge.techtask.nettychat.server.Command.JOIN;

@Component
@RequiredArgsConstructor
public class JoinCommand implements ChatCommand {

	private static final int JOIN_MESSAGE_COUNT = 10;

	private final GroupChannelService groupChannelService;
	private final MessageService messageService;
	private final UserService userService;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		String channelName = requestData.getMessage();

		if (channelName.isBlank()) {
			throw new ChatCommandException("Invalid join command. Message format is " + JOIN.getCommand() + " <channel name>.");
		}

		Optional<String> nameOptional = groupChannelService.getCurrentChannelName(ctx.channel());

		if (nameOptional.isPresent() && channelName.equals(nameOptional.get())) {
			throw new ChatCommandException("You already connected to channel: " + channelName);
		}

		try {
			if (nameOptional.isPresent()) {
				groupChannelService.leaveCurrentChannel(ctx.channel());
			}

			groupChannelService.joinChannel(ctx.channel(), channelName);
			updateActiveUserLastChannel(ctx, channelName);

			List<MessageData> messages = messageService.getLastNMessages(channelName, JOIN_MESSAGE_COUNT);

			StringBuilder sb = new StringBuilder("Successfully joined to the channel: ");
			sb.append(channelName);
			sb.append('\n');

			if (messages.isEmpty()) {
				sb.append("No messages yet.\n");
				ctx.writeAndFlush(sb.toString());
				return;
			}

			for (MessageData md : messages) {
				sb.append(md.toString());
				sb.append('\n');
			}

			ctx.writeAndFlush(sb.toString());
		} catch (GroupLimitExceededException e) {
			throw new ChatCommandException("Error during join command: channel is full.");
		}
	}

	private void updateActiveUserLastChannel(ChannelHandlerContext ctx, String channelName) {
		ChatUser chatUser = ChannelAttributeUtils.getChannelValue(ctx.channel(), ACTIVE_USER);
		chatUser.setLastChannel(channelName);

		userService.storeChatUser(chatUser);
	}
}
