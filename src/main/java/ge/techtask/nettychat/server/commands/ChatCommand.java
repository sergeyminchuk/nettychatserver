package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.data.RequestData;
import io.netty.channel.ChannelHandlerContext;

public interface ChatCommand {
	void process(ChannelHandlerContext ctx, RequestData requestData);
}
