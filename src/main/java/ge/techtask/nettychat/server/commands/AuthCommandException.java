package ge.techtask.nettychat.server.commands;

public class AuthCommandException extends RuntimeException {

	private static final long serialVersionUID = 5969961529280919686L;

	public AuthCommandException() {
	}

	public AuthCommandException(String message) {
		super(message);
	}
}
