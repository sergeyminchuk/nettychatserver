package ge.techtask.nettychat.server.commands;

public class ChatCommandException extends RuntimeException {
	private static final long serialVersionUID = -1676129968998347409L;

	public ChatCommandException() {
	}

	public ChatCommandException(String message) {
		super(message);
	}
}
