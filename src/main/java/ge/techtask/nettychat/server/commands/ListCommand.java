package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.GroupChannelService;
import io.netty.channel.ChannelHandlerContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ListCommand implements ChatCommand {

	private final GroupChannelService groupChannelService;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		List<String> channels = groupChannelService.getChannelNames();

		if (channels.isEmpty()) {
			ctx.writeAndFlush("There are no channels yet.");
			return;
		}

		StringBuilder sb = new StringBuilder("List of channels:\n");
		for (String channel : channels) {
			sb.append("- ");
			sb.append(channel);
			sb.append('\n');
		}

		ctx.writeAndFlush(sb.toString());
	}
}
