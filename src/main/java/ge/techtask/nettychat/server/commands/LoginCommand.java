package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.data.ChatUser;
import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.AuthService;
import ge.techtask.nettychat.service.UserService;
import ge.techtask.nettychat.utils.ChannelAttributeUtils;
import io.netty.channel.ChannelHandlerContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import static ge.techtask.nettychat.server.ChannelAttribute.ACTIVE_USER;
import static ge.techtask.nettychat.server.Command.LOGIN;

@Component
@RequiredArgsConstructor
public class LoginCommand implements ChatCommand {

	private static final String SALT = "adF348r9ad%&^*&";
	private static final int NAME_IND = 0;
	private static final int PWD_IND = 1;

	private final UserService userService;
	private final AuthService authService;
	private final JoinCommand joinCommand;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		ChatUser inputChatUser = parseChatUser(requestData.getMessage());

		Optional<ChatUser> userOptional = userService.getChatUser(inputChatUser.getName());

		if (userOptional.isEmpty()) {
			hashPassword(inputChatUser);
			userService.storeChatUser(inputChatUser);
			ctx.writeAndFlush("User created successfully");
			return;
		}

		ChatUser chatUser = userOptional.get();
		String originalPassword = hashPassword(chatUser);
		String inputPassword = hashPassword(inputChatUser);

		if (!originalPassword.equals(inputPassword)) {
			throw new ChatCommandException("Invalid name or password");
		}

		authService.login(ctx.channel().id(), chatUser);
		ctx.writeAndFlush("User logged in successfully");
		ChannelAttributeUtils.setChannelValue(ctx.channel(), ACTIVE_USER, chatUser);

		if (chatUser.getLastChannel() == null) {
			return;
		}

//		redirect to join command with updated message
		requestData.setMessage(chatUser.getLastChannel());
		joinCommand.process(ctx, requestData);
	}

	private static String hashPassword(ChatUser chatUser) {
		MessageDigest md;

		try {
			md = MessageDigest.getInstance("SHA-512");
			md.update(SALT.getBytes(StandardCharsets.UTF_8));
			md.update(chatUser.getName().getBytes(StandardCharsets.UTF_8));

			byte[] hashedPassword = md.digest(chatUser.getPassword().getBytes(StandardCharsets.UTF_8));
			return new String(hashedPassword);
		} catch (NoSuchAlgorithmException ignored) {
			throw new ChatCommandException("Error occurred during " + LOGIN.getCommand() + " command");
		}
	}

	private static ChatUser parseChatUser(String message) {
		String[] split = message.split(" ");

		if (split.length < 2 || split[NAME_IND].isBlank() || split[PWD_IND].isBlank()) {
			throw new ChatCommandException("Invalid login command. Message format is " + LOGIN.getCommand() + " <user> <password>");
		}

		return ChatUser
				.builder()
				.name(split[NAME_IND])
				.password(split[PWD_IND])
				.build();
	}
}
