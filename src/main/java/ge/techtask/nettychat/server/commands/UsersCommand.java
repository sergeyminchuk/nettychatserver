package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.data.ChatUser;
import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.AuthService;
import ge.techtask.nettychat.service.GroupChannelService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UsersCommand implements ChatCommand {

	private final GroupChannelService groupChannelService;
	private final AuthService authService;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		Optional<String> channelNameOptional = groupChannelService.getCurrentChannelName(ctx.channel());

		if (channelNameOptional.isEmpty()) {
			throw new ChatCommandException("You are not a member of any of the channels.");
		}

		String channelName = channelNameOptional.get();
		List<Channel> connections = groupChannelService.getConnectionsInChannel(channelName);

		Collection<ChatUser> uniqueUsers = connections.stream()
				.map(channel -> authService.getActiveUser(channel.id()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toMap(ChatUser::getName, u -> u, (u1, u2) -> u1)).values();

		if (uniqueUsers.isEmpty()) {
			ctx.writeAndFlush("There are no users in the channel yet.");
			return;
		}

		StringBuilder sb = new StringBuilder("List of users:\n");
		for (ChatUser userName : uniqueUsers) {
			sb.append("- ");
			sb.append(userName.getName());
			sb.append('\n');
		}

		ctx.writeAndFlush(sb.toString());
	}
}
