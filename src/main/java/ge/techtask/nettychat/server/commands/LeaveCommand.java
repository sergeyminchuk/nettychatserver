package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.GroupChannelService;
import io.netty.channel.ChannelHandlerContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class LeaveCommand implements ChatCommand {

	private final GroupChannelService groupChannelService;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		Optional<String> currentChannel = groupChannelService.getCurrentChannelName(ctx.channel());
		boolean left = groupChannelService.leaveCurrentChannel(ctx.channel());

		if (!left) {
			throw new ChatCommandException("You are not a member of any of the channels.");
		}

		currentChannel.ifPresent(s -> ctx.writeAndFlush("You left from the channel: " + s + '.'));
	}
}
