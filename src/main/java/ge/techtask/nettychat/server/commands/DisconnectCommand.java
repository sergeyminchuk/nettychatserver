package ge.techtask.nettychat.server.commands;

import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.service.DisconnectService;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DisconnectCommand implements ChatCommand {

	private final DisconnectService disconnectService;

	@Override
	public void process(ChannelHandlerContext ctx, RequestData requestData) {
		disconnectService.disconnect(ctx.channel());

		ChannelPromise channelPromise = ctx.newPromise();
		channelPromise.addListener(p -> ctx.channel().close());

		ctx.writeAndFlush("You are disconnected from the server.", channelPromise);
	}
}
