package ge.techtask.nettychat.server;

import ge.techtask.nettychat.data.RequestData;
import ge.techtask.nettychat.server.commands.AuthCommandException;
import ge.techtask.nettychat.server.commands.ChatCommand;
import ge.techtask.nettychat.service.AuthService;
import ge.techtask.nettychat.service.DisconnectService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

import static ge.techtask.nettychat.server.Command.DISCONNECT;
import static ge.techtask.nettychat.server.Command.LOGIN;

@Component
@RequiredArgsConstructor
@ChannelHandler.Sharable
public class ChatChannelHandler extends SimpleChannelInboundHandler<RequestData> {

	private final AuthService authService;
	private final DisconnectService disconnectService;

	private final ChatCommand loginCommand;
	private final ChatCommand joinCommand;
	private final ChatCommand leaveCommand;
	private final ChatCommand disconnectCommand;
	private final ChatCommand listCommand;
	private final ChatCommand usersCommand;
	private final ChatCommand messageCommand;

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		System.out.println("client connected to server: " + ctx.channel().id());
		ctx.writeAndFlush("Welcome to chat server!");
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) {
		System.out.println("client disconnected server: " + ctx.channel().id());
		disconnectService.disconnect(ctx.channel());
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		ctx.writeAndFlush(cause.getMessage());
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, RequestData requestData) {
		if (Stream.of(LOGIN, DISCONNECT).noneMatch(command -> command.equals(requestData.getCommand()))) {
			boolean valid = authService.isValid(ctx.channel().id());

			if (!valid) {
				throw new AuthCommandException("Not authorized. Use " + LOGIN.getCommand() + " command");
			}
		}

		switch (requestData.getCommand()) {
			case LOGIN:
				loginCommand.process(ctx, requestData);
				break;
			case JOIN:
				joinCommand.process(ctx, requestData);
				break;
			case LEAVE:
				leaveCommand.process(ctx, requestData);
				break;
			case DISCONNECT:
				disconnectCommand.process(ctx, requestData);
				break;
			case LIST:
				listCommand.process(ctx, requestData);
				break;
			case USERS:
				usersCommand.process(ctx, requestData);
				break;
			case MESSAGE:
				messageCommand.process(ctx, requestData);
				break;
		}
	}
}
