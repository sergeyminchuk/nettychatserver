package ge.techtask.nettychat.data;

import ge.techtask.nettychat.server.Command;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Pattern;

public class RequestDataDecoder extends ByteToMessageDecoder {

	private static final Charset UTF_8 = StandardCharsets.UTF_8;
	private static final Pattern SPACE_DELIMITER = Pattern.compile(" ");
	private static final char COMMAND_CHARACTER = '/';

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
		String rawMessage = in.toString(UTF_8);
		in.readerIndex(in.readerIndex() + in.readableBytes());

		RequestData requestData = new RequestData();

		if (rawMessage.charAt(0) == COMMAND_CHARACTER) {
			String[] split = SPACE_DELIMITER.split(rawMessage, 2);

			String commandName = split[0];
			Command command = Command.getCommandByName(commandName);
			requestData.setCommand(command);

			if (split.length > 1) {
				String message = split[1];
				requestData.setMessage(message);
			} else {
				requestData.setMessage("");
			}
		} else {
			requestData.setCommand(Command.MESSAGE);
			requestData.setMessage(rawMessage);
		}

		out.add(requestData);
	}
}
