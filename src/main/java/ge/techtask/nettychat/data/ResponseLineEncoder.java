package ge.techtask.nettychat.data;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.string.StringEncoder;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ResponseLineEncoder extends StringEncoder {

	private static final String DEFAULT_LINE_SEPARATOR = System.lineSeparator();
	private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
	private final String lineSeparator;

	public ResponseLineEncoder() {
		this(DEFAULT_CHARSET, DEFAULT_LINE_SEPARATOR);
	}

	public ResponseLineEncoder(String lineSeparator) {
		this(DEFAULT_CHARSET, lineSeparator);
	}

	public ResponseLineEncoder(Charset charset, String lineSeparator) {
		super(charset);
		this.lineSeparator = lineSeparator;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, CharSequence msg, List<Object> out) throws Exception {
		if (msg.length() == 0) {
			return;
		}

		super.encode(ctx, msg + lineSeparator, out);
	}
}
