package ge.techtask.nettychat.data;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class MessageData {
	private String userName;
	private String message;

	@Override
	public String toString() {
		return String.format("[%s] - %s", userName, message);
	}
}
