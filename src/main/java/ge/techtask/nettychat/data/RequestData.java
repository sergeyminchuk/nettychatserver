package ge.techtask.nettychat.data;

import ge.techtask.nettychat.server.Command;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestData {
	private Command command;
	private String message;
}
