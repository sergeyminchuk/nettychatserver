package ge.techtask.nettychat.data;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ChatUser {
	private String name;
	private String password;
	private String lastChannel;
}
