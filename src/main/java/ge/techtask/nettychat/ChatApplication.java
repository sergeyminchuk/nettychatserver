package ge.techtask.nettychat;

import ge.techtask.nettychat.server.ChatServer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class ChatApplication {

	public static void main(String[] args) throws InterruptedException {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext()) {
			context.register(ChatApplication.class);
			context.refresh();

			ChatServer chatServer = context.getBean(ChatServer.class);
			chatServer.run();
		}
	}
}
