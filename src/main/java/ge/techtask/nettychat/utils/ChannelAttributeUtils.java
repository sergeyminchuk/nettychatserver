package ge.techtask.nettychat.utils;

import ge.techtask.nettychat.server.ChannelAttribute;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ChannelAttributeUtils {

	public <T> T getChannelValue(Channel channel, ChannelAttribute channelAttribute) {
		AttributeKey<T> attributeKey = AttributeKey.valueOf(channelAttribute.name());
		return channel.attr(attributeKey).get();
	}

	public <T> void setChannelValue(Channel channel, ChannelAttribute channelAttribute, T value) {
		AttributeKey<T> attributeKey = AttributeKey.valueOf(channelAttribute.name());
		channel.attr(attributeKey).set(value);
	}
}
