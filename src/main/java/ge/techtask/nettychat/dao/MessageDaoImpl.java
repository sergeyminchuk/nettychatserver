package ge.techtask.nettychat.dao;

import ge.techtask.nettychat.data.MessageData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MessageDaoImpl implements MessageDao {

	private Map<String, List<MessageData>> channelMessages = new ConcurrentHashMap<>();

	@Override
	public MessageData save(String channelName, MessageData messageData) {
		channelMessages.putIfAbsent(channelName, new ArrayList<>());

		channelMessages.computeIfPresent(channelName, (name, messages) -> {
			messages.add(messageData);
			return messages;
		});

		return messageData;
	}

	@Override
	public List<MessageData> findLastNMessages(String channelName, int n) {
		List<MessageData> lastN = new ArrayList<>(n);

		channelMessages.computeIfPresent(channelName, (name, messages) -> {
			int size = messages.size();
			lastN.addAll(messages.subList(Math.max(size - n, 0), size));
			return messages;
		});

		return lastN;
	}
}
