package ge.techtask.nettychat.dao;

import ge.techtask.nettychat.data.ChatUser;

public interface UserDao {
	ChatUser find(String name);
	ChatUser saveOrUpdate(ChatUser chatUser);
}
