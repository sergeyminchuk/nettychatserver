package ge.techtask.nettychat.dao;

import ge.techtask.nettychat.data.MessageData;

import java.util.List;

public interface MessageDao {
	MessageData save(String channelName, MessageData messageData);
	List<MessageData> findLastNMessages(String channelName, int n);
}
