package ge.techtask.nettychat.dao;

import ge.techtask.nettychat.data.ChatUser;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class UserDaoImpl implements UserDao {

	private final Map<String, ChatUser> inMemoryUserStorage = new ConcurrentHashMap<>();

	@Override
	public ChatUser find(String name) {
		return inMemoryUserStorage.get(name);
	}

	@Override
	public ChatUser saveOrUpdate(ChatUser chatUser) {
		return inMemoryUserStorage.put(chatUser.getName(), chatUser);
	}
}
