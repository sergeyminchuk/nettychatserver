package ge.techtask.nettychat.concurrent;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChannelGroupFixed {
	private static final int DEFAULT_MAX_SIZE = 10;
	private static final long DEFAULT_TRY_DURATION_SEC = 1L;

	private final ChannelGroup channelGroup;
	private final int maxSize;
	private final Lock fixedAddLock;

	public ChannelGroupFixed() {
		this(DEFAULT_MAX_SIZE);
	}

	public ChannelGroupFixed(int maxSize) {
		this(maxSize, new DefaultChannelGroup(GlobalEventExecutor.INSTANCE));
	}

	public ChannelGroupFixed(int maxSize, ChannelGroup channelGroup) {
		this.channelGroup = channelGroup;
		this.maxSize = maxSize;

		this.fixedAddLock = new ReentrantLock();
	}

	public boolean add(Channel channel) throws GroupLimitExceededException {
		return add(channel, DEFAULT_TRY_DURATION_SEC, TimeUnit.SECONDS);
	}

	public boolean add(Channel channel, long tryDuration, TimeUnit timeUnit) throws GroupLimitExceededException {
		try {
			if (fixedAddLock.tryLock(tryDuration, timeUnit)) {
				try {
					if (channelGroup.size() < maxSize) {
						return channelGroup.add(channel);
					} else {
						throw new GroupLimitExceededException("channel group is full");
					}
				} finally {
					fixedAddLock.unlock();
				}
			} else {
				throw new IllegalArgumentException("channel group is busy");
			}
		} catch (InterruptedException e) {
			throw new IllegalArgumentException("error during adding channel to group", e);
		}
	}

	public int size() {
		return channelGroup.size();
	}

	public boolean remove(Channel channel) {
		return channelGroup.remove(channel);
	}

	public List<Channel> getChannels() {
		return new ArrayList<>(channelGroup);
	}

	//	don't use ChannelGroupFuture.getGroup() directly to make sure you don't exceed the limit of group
	public ChannelGroupFuture write(Object msg) {
		return channelGroup.writeAndFlush(msg);
	}
}
