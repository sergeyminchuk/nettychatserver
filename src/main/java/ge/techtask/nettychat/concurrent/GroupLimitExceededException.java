package ge.techtask.nettychat.concurrent;

public class GroupLimitExceededException extends Exception {
	private static final long serialVersionUID = -5920736905465369622L;

	public GroupLimitExceededException() {
	}

	public GroupLimitExceededException(String message) {
		super(message);
	}
}
