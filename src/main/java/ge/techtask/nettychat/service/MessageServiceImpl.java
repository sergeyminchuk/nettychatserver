package ge.techtask.nettychat.service;

import ge.techtask.nettychat.dao.MessageDao;
import ge.techtask.nettychat.data.MessageData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

	private final MessageDao messageDao;

	@Override
	public MessageData store(String channelName, MessageData messageData) {
		return messageDao.save(channelName, messageData);
	}

	@Override
	public List<MessageData> getLastNMessages(String channelName, int n) {
		return messageDao.findLastNMessages(channelName, n);
	}
}
