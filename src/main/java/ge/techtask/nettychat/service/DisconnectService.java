package ge.techtask.nettychat.service;

import io.netty.channel.Channel;

public interface DisconnectService {
	void disconnect(Channel channel);
}
