package ge.techtask.nettychat.service;

import ge.techtask.nettychat.dao.UserDao;
import ge.techtask.nettychat.data.ChatUser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserDao userDao;

	@Override
	public Optional<ChatUser> getChatUser(String name) {
		return Optional.ofNullable(userDao.find(name));
	}

	@Override
	public ChatUser storeChatUser(ChatUser chatUser) {
		return userDao.saveOrUpdate(chatUser);
	}
}
