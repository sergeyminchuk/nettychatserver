package ge.techtask.nettychat.service;

import io.netty.channel.Channel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DisconnectServiceImpl implements DisconnectService {

	private final AuthService authService;
	private final GroupChannelService groupChannelService;

	@Override
	public void disconnect(Channel channel) {
		groupChannelService.leaveCurrentChannel(channel);
		authService.logout(channel.id());
	}
}
