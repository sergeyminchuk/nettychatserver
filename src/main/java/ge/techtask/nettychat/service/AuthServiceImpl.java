package ge.techtask.nettychat.service;

import ge.techtask.nettychat.data.ChatUser;
import io.netty.channel.ChannelId;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class AuthServiceImpl implements AuthService {

	private final Map<ChannelId, ChatUser> sessions = new ConcurrentHashMap<>();

	@Override
	public void login(ChannelId channelId, ChatUser chatUser) {
		sessions.put(channelId, chatUser);
	}

	@Override
	public boolean isValid(ChannelId channelId) {
		return sessions.containsKey(channelId);
	}

	@Override
	public void logout(ChannelId channelId) {
		sessions.remove(channelId);
	}

	@Override
	public Optional<ChatUser> getActiveUser(ChannelId channelId) {
		return Optional.ofNullable(sessions.get(channelId));
	}
}
