package ge.techtask.nettychat.service;

import ge.techtask.nettychat.concurrent.ChannelGroupFixed;
import ge.techtask.nettychat.concurrent.GroupLimitExceededException;
import io.netty.channel.Channel;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface GroupChannelService {
	void joinChannel(Channel channel, String name) throws GroupLimitExceededException;
	Optional<String> getCurrentChannelName(Channel channel);
	Optional<Map.Entry<String, ChannelGroupFixed>> getCurrentGroup(Channel channel);
	boolean leaveCurrentChannel(Channel channel);
	List<String> getChannelNames();
	List<Channel> getConnectionsInChannel(String name);
}
