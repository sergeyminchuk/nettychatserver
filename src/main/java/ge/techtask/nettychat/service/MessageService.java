package ge.techtask.nettychat.service;

import ge.techtask.nettychat.data.MessageData;

import java.util.List;

public interface MessageService {
	MessageData store(String channelName, MessageData messageData);
	List<MessageData> getLastNMessages(String channelName, int n);
}
