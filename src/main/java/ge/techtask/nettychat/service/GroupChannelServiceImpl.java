package ge.techtask.nettychat.service;

import ge.techtask.nettychat.concurrent.ChannelGroupFixed;
import ge.techtask.nettychat.concurrent.GroupLimitExceededException;
import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class GroupChannelServiceImpl implements GroupChannelService {

	private final Map<String, ChannelGroupFixed> nameToConnectionGroupMap = new ConcurrentHashMap<>();
	private final Map<ChannelId, String> channelToNameMap = new ConcurrentHashMap<>();

	@Override
	public void joinChannel(Channel channel, String name) throws GroupLimitExceededException {
		nameToConnectionGroupMap.putIfAbsent(name, new ChannelGroupFixed());
		nameToConnectionGroupMap.get(name).add(channel);
		channelToNameMap.put(channel.id(), name);
	}

	@Override
	public Optional<String> getCurrentChannelName(Channel channel) {
		return Optional.ofNullable(channelToNameMap.get(channel.id()));
	}

	@Override
	public Optional<Map.Entry<String, ChannelGroupFixed>> getCurrentGroup(Channel channel) {
		Optional<String> nameOptional = getCurrentChannelName(channel);

		if (nameOptional.isEmpty()) {
			return Optional.empty();
		}

		ChannelGroupFixed channelGroupFixed = nameToConnectionGroupMap.get(nameOptional.get());

		if (channelGroupFixed == null) {
			return Optional.empty();
		}

		return Optional.of(Map.entry(nameOptional.get(), channelGroupFixed));
	}

	@Override
	public boolean leaveCurrentChannel(Channel channel) {
		Optional<String> channelOptional = getCurrentChannelName(channel);

		if (channelOptional.isEmpty()) {
			return false;
		}

		Optional<ChannelGroupFixed> channelGroup = Optional.ofNullable(nameToConnectionGroupMap.get(channelOptional.get()));
		channelGroup.ifPresent(channelGroupFixed -> channelGroupFixed.remove(channel));
		channelToNameMap.remove(channel.id());

		return true;
	}

	@Override
	public List<String> getChannelNames() {
		return new ArrayList<>(nameToConnectionGroupMap.keySet());
	}

	@Override
	public List<Channel> getConnectionsInChannel(String name) {
		Optional<ChannelGroupFixed> channelGroup = Optional.ofNullable(nameToConnectionGroupMap.get(name));

		if (channelGroup.isPresent()) {
			return channelGroup.get().getChannels();
		}

		return Collections.emptyList();
	}
}
