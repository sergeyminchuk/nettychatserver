package ge.techtask.nettychat.service;

import ge.techtask.nettychat.data.ChatUser;

import java.util.Optional;

public interface UserService {
	Optional<ChatUser> getChatUser(String name);
	ChatUser storeChatUser(ChatUser chatUser);
}
