package ge.techtask.nettychat.service;

import ge.techtask.nettychat.data.ChatUser;
import io.netty.channel.ChannelId;

import java.util.Optional;

public interface AuthService {
	void login(ChannelId channelId, ChatUser chatUser);
	void logout(ChannelId channelId);
	boolean isValid(ChannelId channelId);
	Optional<ChatUser> getActiveUser(ChannelId channelId);
}
